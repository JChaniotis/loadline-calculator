(function () {
    'use strict';
    /*globals 
    Cango, 
    Cobj,
    colorTables, 
    tubeTypes, 
    setupDragHandlers, 
    exposeGlobal, 
    updateResults, 
    stateManager, 
    applyDefaultRanges,
    shapeDefs*/

    let tube;
    let backLayer;
    let frontLayer;

    const colorTable = colorTables.fancy;

    /**
     * Conserve aspect ratio of the orignal region. Useful when shrinking/enlarging
     * images to fit into a certain area.
     *
     * @param {Number} srcWidth Source area width
     * @param {Number} srcHeight Source area height
     * @param {Number} maxWidth Fittable area maximum available width
     * @param {Number} maxHeight Fittable area maximum available height
     * @return {Object} { width, heigth }
     */
    const calculateAspectRatioFit = (srcWidth, srcHeight, maxWidth, maxHeight) => {

        let ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

        return {
            width: srcWidth * ratio,
            height: srcHeight * ratio
        };
    };

    const setCanvasDimensions = (canvas) => {

        let width = canvas.parentElement.offsetWidth - 3;
        let height = canvas.parentElement.offsetHeight - 3;

        let r = calculateAspectRatioFit(100, 70, width, height);

        canvas.setAttribute('width', r.width);
        canvas.setAttribute('height', r.height);
        canvas.style.height = canvas.height + 'px';
        canvas.style.width = canvas.width + 'px';
    };


    const initializeDraggableCursors = (tube, drawFn) => {

        // shapes of each cursor
        const arrowUp = ['M', 0, 0, 'L', 2.5, -0.5, 'L', -2.5, -0.5, 'z'];
        const arrowDown = ['M', 0, 0, 'L', 2.5, 0.5, 'L', -2.5, 0.5, 'z'];
        //const arrowRight = ['M', 0, 0, 'L', -5, 0.25, 'L', -5, -0.25, 'z'];
        const arrowLeft = ['M', 0, 0, 'L', 5, 0.25, 'L', 5, -0.25, 'z'];
        const arrowLeftLong = ['M', 0, 0, 'L', 5, 0.25, 'L', 20, 0.25, 'L', 20, -0.25, 'L', 5, -0.25, 'z'];
        const arrowRightLong = ['M', 0, 0, 'L', -5, 0.25, 'L', -20, 0.25, 'L', -20, -0.25, 'L', -5, -0.25, 'z'];
        //const arrowUpLong = ['M', 0, 0, 'L', 2.5, -0.5, 'L', 2.5, -20, 'L', -2.5, -20, 'L', -2.5, -0.5, 'z'];
        const cursors = {
            // bottom X axis, voltage selector
            tickU: new Cobj(arrowUp, 'SHAPE', {
                fillColor: colorTable.c_tickP,
                iso: false
            }),
            // left Y axis, current selector
            tickI: new Cobj(arrowRightLong, 'SHAPE', {
                fillColor: colorTable.c_tickP,
                iso: false
            }),
            // top X axis, bias selector
            tickP: new Cobj(arrowDown, 'SHAPE', {
                fillColor: colorTable.c_tickP,
                iso: false
            }),
            // top X axis, input voltage selector
            tickA: new Cobj(arrowUp, 'SHAPE', {
                fillColor: colorTable.c_tickA,
                iso: false
            }),
            // bottom X axis, shift operating point selector
            tickUP: new Cobj(arrowDown, 'SHAPE', {
                fillColor: colorTable.c_tickUP,
                iso: false
            }),
            // left Y axis, shift operating point selector
            tickIP: new Cobj(arrowLeft, 'SHAPE', {
                fillColor: colorTable.c_tickIP,
                iso: false
            }),
            tickIAlineMin: new Cobj(arrowLeftLong, 'SHAPE', {
                fillColor: colorTable.c_ialine,
                iso: false
            })
        };

        // Scale all cursors
        const xcoef = tube.uamax / 210;
        const ycoef = tube.iamax / 14;
        Object.keys(cursors).forEach((cursor) => cursors[cursor].scale(xcoef, ycoef));
        const handlers = setupDragHandlers(tube, drawFn.bind(null, cursors));

        const updateState = () => {
            stateManager.data = tube.serialize();
        };

        cursors.tickIP.enableDrag(null, handlers.tickIPDrag, updateState);
        cursors.tickUP.enableDrag(null, handlers.tickUPDrag, updateState);
        cursors.tickA.enableDrag(null, handlers.tickADrag, updateState);
        cursors.tickP.enableDrag(null, handlers.tickPDrag, updateState);
        cursors.tickI.enableDrag(null, handlers.tickIDrag, updateState);
        cursors.tickU.enableDrag(null, handlers.tickUDrag, updateState);
        cursors.tickIAlineMin.enableDrag(null, handlers.tickIAlineMinDrag, updateState);

        drawFn(cursors);
    };

    /**
     * Computes the points of a grid curve
     * @param  {Tube} tube
     * @param  {Number} gridVoltage The grid voltage used for computation
     * @return {Array}             points
     */
    const computeGridCurve = (tube, gridVoltage) => {
        let points = [];

        let x = 0;
        let xmax = tube.getUaMax_Ug(gridVoltage);
        let samples = 100;
        let step = (xmax - x) / samples;

        do {
            points.push(x, 1000 * tube.getIa(gridVoltage, x));
            x += step;
        } while (samples--);

        return points;
    };

    /**
     * Draws the max anode dissipation curve
     * @param  {Tube} tube 
     * @param  {CangoLayer} layer 
     */
    const drawMaxAnodeDissipation = (tube, layer) => {
        let points = [];
        let samples = 100;

        let x = (1000 * tube.padm) / tube.iamax;
        let step = (tube.uamax - x) / samples;

        do {
            points.push(x, 1000 * tube.padm / x);
            x += step;
        } while (samples--);

        layer.drawPath(points, 0, 0, {
            strokeColor: colorTable.c_padm,
            lineWidth: 1
        });

        layer.drawText(tube.padm.toFixed(2) + 'W', points[points.length - 2], points[points.length - 1], {
            fillColor: colorTable.c_padm,
            fontSize: 12,
            lorg: 8
        });
    };


    const drawGridVoltageText = (points, layer, gridVoltage) => {
        const A = {
            x: points[points.length - 6],
            y: points[points.length - 5]
        };

        const B = {
            x: points[points.length - 16],
            y: points[points.length - 15]
        };

        // Uncomment for debug
        /*const C = {
            x: A.x,
            y: B.y
        };

        layer.drawPath([A.x, A.y, B.x, B.y, C.x, C.y, A.x, A.y], 0, 0, {
            strokeColor: 'red',
            lineWidth: 1
        });*/

        // The canvas actual ratio MUST be taken into account
        // when rotating since the axes are not uniform. Took me a few hours...
        const ratio = Math.abs(layer.yscl / layer.xscl);
        const deltaX = (A.x - B.x) / ratio;
        const deltaY = (A.y - B.y);
        const rad = Math.atan2(deltaY, deltaX);
        const deg = rad * (180 / Math.PI);

        const text = new Cobj(gridVoltage.toFixed(2) + 'V', 'TEXT', {
            fillColor: colorTable.c_backText,
            fontSize: 12,
            lorg: 9,
        });
        layer.render(text, A.x, A.y, 1, deg);
    };

    /**
     * Draws the grid voltage curves
     * @param  {Tube} tube
     * @param  {CangoLayer} layer
     */
    const drawGridCurves = (tube, layer) => {
        let points = [];
        let totalCurves = 6;
        let gridVoltage = tube.ugmin;
        let step = Math.abs((tube.ugmin - tube.ugmax) / totalCurves);
        let i;

        for (i = 0; totalCurves > i; i++) {
            points = computeGridCurve(tube, gridVoltage);

            layer.drawPath(points, 0, 0, {
                strokeColor: colorTable.c_backGraph,
            });
            drawGridVoltageText(points, layer, gridVoltage);
            gridVoltage -= step;
            points = [];
        }
    };

    /**
     * Draws the loadline
     * @param  {Tube} tube
     * @param  {CangoLayer} layer
     */
    const drawLoadLine = (tube, layer) => {
        let points = [0, tube.ia, tube.ua, 0];
        layer.drawPath(points, 0, 0, {
            strokeColor: colorTable.c_tickP,
            lineWidth: 2
        });
    };

    const drawQuiescentCurve = (tube, layer) => {
        let points = computeGridCurve(tube, tube.ugp);

        layer.drawPath(points, 0, 0, {
            strokeColor: colorTable.c_frontUagpCurve,
            lineWidth: 2
        });
        drawGridVoltageText(points, layer, tube.ugp);

        /*
            Draw the line that connects the grid voltage selector to its curve
         */
        points = [tube.uagp, tube.iamax, tube.uagp, 1000 * tube.iagp];
        layer.drawPath(points, 0, 0, {
            strokeColor: colorTable.c_frontUagpLine,
            lineWidth: 1
        });
    };

    /**
     * Draws the interactive layer
     */
    const frontLayerDraw = (cursors) => {

        frontLayer.clearCanvas();
        frontLayer.render(cursors.tickU, tube.ua, 0, 1);
        frontLayer.render(cursors.tickI, 0, tube.ia, 1);
        frontLayer.render(cursors.tickP, tube.uagp, tube.iamax, 1);
        frontLayer.render(cursors.tickA, tube.uag1, tube.iamax, 1);
        frontLayer.render(cursors.tickUP, tube.ua, 0, 1);
        frontLayer.render(cursors.tickIP, 0, tube.ia, 1);
        frontLayer.render(cursors.tickIAlineMin, tube.uamax, tube.ialineMin, 0);

        let linePoints = [];
        let textOrigin;


        const tickPtext = new Cobj(tube.uagp.toFixed(1) + 'V', 'TEXT', {
            fillColor: colorTable.c_frontText,
            fontSize: 12,
            lorg: 7
        });

        textOrigin = cursors.tickP.dwgOrg;
        textOrigin.y += cursors.tickP.drawCmds[1].parms[0][1];
        textOrigin.x += cursors.tickP.drawCmds[1].parms[0][0];
        frontLayer.render(tickPtext, textOrigin.x, textOrigin.y);


        drawLoadLine(tube, frontLayer);
        drawQuiescentCurve(tube, frontLayer);

        /*
            Draw the left line that connects the input voltage selector to its curve
         */
        linePoints = [tube.uag1, tube.iamax, tube.uag1, 1e3 * tube.iag1];
        frontLayer.drawPath(linePoints, 0, 0, {
            strokeColor: colorTable.c_frontUag1Line,
            lineWidth: 1
        });

        /*
            Draw the left input voltage curve
         */
        frontLayer.drawPath(computeGridCurve(tube, tube.ug1), 0, 0, {
            strokeColor: colorTable.c_frontUag1Curve,
            lineWidth: 2
        });

        /*
           Draw the right line that connects the input voltage to its curve
         */
        linePoints = [tube.uag2, tube.iamax, tube.uag2, 1e3 * tube.iag2];
        frontLayer.drawPath(linePoints, 0, 0, {
            strokeColor: colorTable.c_frontUag2Line,
            lineWidth: 1
        });

        /*
            Draw the right input voltage curve
         */
        frontLayer.drawPath(computeGridCurve(tube, tube.ug2), 0, 0, {
            strokeColor: colorTable.c_frontUag2Curve,
            lineWidth: 2
        });

        const tickUtext = new Cobj(tube.ua.toFixed(1) + 'V', 'TEXT', {
            fillColor: colorTable.c_frontText,
            fontSize: 12,
            lorg: 1
        });

        textOrigin = cursors.tickU.dwgOrg;
        textOrigin.y += cursors.tickU.drawCmds[1].parms[0][1];
        textOrigin.x += cursors.tickU.drawCmds[1].parms[0][0];
        frontLayer.render(tickUtext, textOrigin.x, textOrigin.y);

        textOrigin = cursors.tickI.dwgOrg;
        textOrigin.x += cursors.tickI.drawCmds[4].parms[0][0];

        frontLayer.drawText(tube.ia.toFixed(2) + 'mA', textOrigin.x, textOrigin.y, {
            fillColor: colorTable.c_frontText,
            fontSize: 12,
            lorg: 6
        });

        linePoints = [0, tube.ialineMin, tube.uamax, tube.ialineMin];
        frontLayer.drawPath(linePoints, 0, 0, {
            strokeColor: colorTable.c_ialine,
            lineWidth: 1
        });


        textOrigin = cursors.tickIAlineMin.dwgOrg;
        textOrigin.x += cursors.tickIAlineMin.drawCmds[4].parms[0][0];

        frontLayer.drawText(tube.ialineMin.toFixed(2) + 'mA', textOrigin.x, textOrigin.y, {
            fillColor: colorTable.c_frontText,
            fontSize: 12,
            lorg: 4
        });

        linePoints = [0, tube.ialineMax, tube.uamax, tube.ialineMax];
        frontLayer.drawPath(linePoints, 0, 0, {
            strokeColor: colorTable.c_ialine,
            lineWidth: 1
        });


        frontLayer.drawText(tube.ialineMax.toFixed(2) + 'mA', textOrigin.x, tube.ialineMax, {
            fillColor: colorTable.c_frontText,
            fontSize: 12,
            lorg: 4
        });

        updateResults(tube);
    };

    const offsetPx = (layer, point, offset) => {
        let p = layer.toPixelCoords(point.x, point.y);
        p.x += offset;
        p.y += offset;
        return layer.toWorldCoords(p.x, p.y);
    };

    const drawAxes = (tube, layer) => {
 
        let box = new Cobj(shapeDefs.rectangle(tube.uamax, tube.iamax), 'PATH', {
            strokeColor: colorTable.c_axesText,
        });

        layer.render(box, tube.uamax / 2, tube.iamax / 2);

        const topLeft = offsetPx(layer, { x: tube.uamin, y: tube.iamax }, -10);
        const bottomRight = offsetPx(layer, { x: tube.uamax, y: tube.iamin }, 10);
        const bottomLeft = {
            x: topLeft.x,
            y: bottomRight.y
        };
        
        layer.drawText(tube.name, tube.uamin, topLeft.y, {
            fillColor: colorTable.c_frontText,
            fontSize: 12,
            lorg: 7,
            //bgfillcolor: 'lime'
        });

        layer.drawText(tube.iamin+ 'mA', bottomLeft.x, tube.iamin, {
            fillColor: colorTable.c_axesText,
            fontSize: 12,
            lorg: 9
        });

        layer.drawPath([bottomLeft.x, tube.iamin, tube.uamin, tube.iamin], 0, 0, {
            strokeColor: colorTable.c_axesText
        });

        layer.drawText(tube.iamax + 'mA', topLeft.x, tube.iamax, {
            fillColor: colorTable.c_axesText,
            fontSize: 12,
            lorg: 3
        });

        layer.drawPath([topLeft.x, tube.iamax, tube.uamin, tube.iamax], 0, 0, {
            strokeColor: colorTable.c_axesText
        });

        layer.drawText(tube.uamin + 'V', tube.uamin, bottomLeft.y, {
            fillColor: colorTable.c_axesText,
            fontSize: 12,
            lorg: 1
        });

        layer.drawPath([tube.uamin, bottomLeft.y, tube.uamin, tube.iamin], 0, 0, {
            strokeColor: colorTable.c_axesText
        });


        layer.drawText(tube.uamax + 'V', tube.uamax, bottomRight.y, {
            fillColor: colorTable.c_axesText,
            fontSize: 12,
            lorg: 3
        });

        layer.drawPath([tube.uamax, bottomRight.y, tube.uamax, tube.iamin], 0, 0, {
            strokeColor: colorTable.c_axesText
        });

    };

    const initialize = (tubeData, operatingParams, canvas) => {
        setCanvasDimensions(canvas);
        // Reinit logic
        if (tubeData) {
            tube = ('T' === tubeData[0]) ? new tubeTypes.TRIODE(tubeData, operatingParams) : new tubeTypes.PENTODE(tubeData, operatingParams);
            document.title = tubeData[1];
        }

        if ('object' === typeof backLayer) {
            backLayer.clearCanvas();
            backLayer.deleteAllLayers();
        }

        backLayer = new Cango(canvas.id);
        // Set the background color
        backLayer.fillGridbox(colorTable.c_backLayer);
        backLayer.setGridboxRHC(8, 7, 84, 56);
        backLayer.setWorldCoords(0, 0, tube.uamax, tube.iamax);

        drawAxes(tube, backLayer);

        if ('object' === typeof frontLayer) {
            frontLayer.clearCanvas();
            frontLayer.deleteAllLayers();
        }

        // Create the 'front' interactive layer
        frontLayer = new Cango(backLayer.createLayer());
        frontLayer.dupCtx(backLayer);

        applyDefaultRanges(tube.serialize().modelData);
        drawGridCurves(tube, backLayer);
        drawMaxAnodeDissipation(tube, backLayer);
        initializeDraggableCursors(tube, frontLayerDraw);
        stateManager.data = tube.serialize();
    };

    exposeGlobal('simulator', {
        initialize
    });
})();