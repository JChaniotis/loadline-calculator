(function () {
    'use strict';
    /*globals simulator, tubeModelData, domAccess, stateManager*/

    /**
     * The first function that is being called
     */
    const init = () => {
        window.addEventListener('resize', function () {
            // Reinitialize
            simulator.initialize(null, null, domAccess.canvas);
        });

        const selectedTube = stateManager.data && stateManager.data.modelData || tubeModelData[0];
        const operatingParams = stateManager.data && stateManager.data.operatingParams || null;

        // Populate the dropdown menu
        let tubeList = domAccess.tubeList;
        tubeModelData.forEach((tubeModel, index) => {
            let entry = document.createElement('option');
            entry.value = index.toString();
            entry.text = tubeModel[1];
            tubeList.appendChild(entry);
            if (selectedTube[1] === tubeModel[1]) {
                tubeList.value = index;
            }
        });

        simulator.initialize(selectedTube, operatingParams, domAccess.canvas);
    };

    init();

})();