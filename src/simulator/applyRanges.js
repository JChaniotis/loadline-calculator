(function () {
    'use strict';
    /*globals exposeGlobal, domAccess, simulator, tubeModelData*/

    const applyDefaultRanges = (tube) => {
        // Reset to default values
        domAccess.parallelTubes.value = tube[2].toString();
        domAccess.maxAnodeVoltage.value = tube[10].toString();
        domAccess.maxAnodeCurrent.value = tube[12].toString();
        domAccess.minGridVoltage.value = tube[13].toString();
        domAccess.maxGridVoltage.value = tube[14].toString();
        domAccess.lowFrequencyLimit.value = '20';
        domAccess.lowFrequencyDistortion.value = '3.0';
        domAccess.maxFrequencyLimit.value = '20000';
        domAccess.maxFrequencyDistortion.value = '3.0';
        domAccess.transformerEfficiency.value = '0.85';
        domAccess.loudspeakerImpedance.value = '4.0';
        if ('T' === tube[0]) {
            domAccess.screenVoltage.value = '---';
        } else {
            domAccess.screenVoltage.value = tube[15].toString();
        }
    };

    /**
     * Check if a number is not valid
     * @param  {*} val The aluse to be checked
     * @return {Boolean}
     */
    const isNotValidNumber = (val) => (val.length === 0 || isNaN(val));

    /**
     * Read, validate and initialize the simulator with customized model parameters
     */
    const applyRanges = () => {

        // Get a reference of the currently selected tube model and clone it
        let tubeList = domAccess.tubeList;
        let index = parseInt(tubeList.options[tubeList.selectedIndex].value);
        let selectedTube = tubeModelData[index].slice(0);
        let type = selectedTube[0];

        // Reference all the required DOM elements
        let parallelTubes = domAccess.parallelTubes.value;
        let maxAnodeVoltage = domAccess.maxAnodeVoltage.value;
        let maxAnodeCurrent = domAccess.maxAnodeCurrent.value;
        let minGridVoltage = domAccess.minGridVoltage.value;
        let maxGridVoltage = domAccess.maxGridVoltage.value;
        let lowFrequencyLimit = domAccess.lowFrequencyLimit.value;
        let lowFrequencyDistortion = domAccess.lowFrequencyDistortion.value;
        let maxFrequencyLimit = domAccess.maxFrequencyLimit.value;
        let maxFrequencyDistortion = domAccess.maxFrequencyDistortion.value;
        let transformerEfficiency = domAccess.transformerEfficiency.value;
        let loudspeakerImpedance = domAccess.loudspeakerImpedance.value;
        let screenVoltage = domAccess.screenVoltage.value;

        let hasError = false;

        // Validate everything
        if (isNotValidNumber(parallelTubes) || parseInt(parallelTubes) < 1 || parseInt(parallelTubes) > 8) {
            hasError = true;
        }
        if (isNotValidNumber(maxAnodeVoltage) || parseFloat(maxAnodeVoltage) <= 0.1 * selectedTube[10] || parseFloat(maxAnodeVoltage) > 10 * selectedTube[10]) {
            hasError = true;
        }
        if (isNotValidNumber(maxAnodeCurrent) || parseFloat(maxAnodeCurrent) <= 0.1 * selectedTube[12] || parseFloat(maxAnodeCurrent) > 10 * selectedTube[12]) {
            hasError = true;
        }
        if (isNotValidNumber(minGridVoltage) || parseFloat(minGridVoltage) < parseFloat(maxGridVoltage)) {
            hasError = true;
        }
        if (isNotValidNumber(maxGridVoltage) || parseFloat(minGridVoltage) < parseFloat(maxGridVoltage)) {
            hasError = true;
        }
        if (isNotValidNumber(lowFrequencyLimit) || parseFloat(lowFrequencyLimit) >= parseFloat(maxFrequencyLimit)) {
            hasError = true;
        }
        if (isNotValidNumber(lowFrequencyDistortion) || parseFloat(lowFrequencyDistortion) <= 0) {
            hasError = true;
        }
        if (isNotValidNumber(maxFrequencyLimit)|| parseFloat(maxFrequencyLimit) <= parseFloat(lowFrequencyLimit)) {
            hasError = true;
        }
        if (isNotValidNumber(maxFrequencyDistortion) || parseFloat(maxFrequencyDistortion) <= 0) {
            hasError = true;
        }
        if (isNotValidNumber(transformerEfficiency) || parseFloat(transformerEfficiency) < 0.1 || parseFloat(transformerEfficiency) > 1) {
            hasError = true;
        }
        if (isNotValidNumber(loudspeakerImpedance) || parseFloat(loudspeakerImpedance) < 1 || parseFloat(loudspeakerImpedance) > 1e5) {
            hasError = true;
        }
        if (type === 'P' && (isNotValidNumber(screenVoltage) || parseFloat(screenVoltage) > parseFloat(maxAnodeVoltage))) {
            hasError = true;
        }

        if (hasError) {
           applyDefaultRanges(selectedTube);
        } else {
            // Update the model parameters and initialize the simulator
            selectedTube[2] = parseInt(parallelTubes);
            selectedTube[10] = parseFloat(maxAnodeVoltage);
            selectedTube[12] = parseFloat(maxAnodeCurrent);
            selectedTube[13] = parseFloat(minGridVoltage);
            selectedTube[14] = parseFloat(maxGridVoltage);
            if ('T' === type) {

                selectedTube[15] = 0;
            } else {
                selectedTube[15] = parseFloat(screenVoltage);
            }
            simulator.initialize(selectedTube, null, domAccess.canvas);
        }

    };

    const resetRanges = () => {
        // Get a reference of the currently selected tube model and clone it
        let tubeList = domAccess.tubeList;
        let index = parseInt(tubeList.options[tubeList.selectedIndex].value);
        let selectedTube = tubeModelData[index].slice(0);
        applyDefaultRanges(selectedTube);
        simulator.initialize(selectedTube, null, domAccess.canvas);
    };

    exposeGlobal('applyDefaultRanges', applyDefaultRanges);
    exposeGlobal('applyRanges', applyRanges);
    exposeGlobal('resetRanges', resetRanges);
})();