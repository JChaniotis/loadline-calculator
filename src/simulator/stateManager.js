(function () {
    'use strict';
    /*globals exposeGlobal*/

    //from: http://stackoverflow.com/a/901144
    function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, '\\$&');
        let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) {
            return null;
        }
        if (!results[2]) {
            return '';
        }
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    class StateManager {
        constructor() {
            //console.dir(this.state);
        }


        get data() {
            let state = getParameterByName('d');

            try {
                state = JSON.parse(state);
                //TODO: some validation
            } catch (e) {
                return null;
            }
            return state;
        }

        set data(state) {
            history.replaceState(null, '', '?d=' + JSON.stringify(state));
        }

    }

    exposeGlobal('stateManager', new StateManager());

})();