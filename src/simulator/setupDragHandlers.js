(function () {
    'use strict';

    /*globals exposeGlobal*/

    const setupDragHandlers = (tube, frontLayerDraw) => {

        const updateMaxCurrentLine = () => {
            const currentMargin = (1000 * tube.iagp) - tube.ialineMin;
            if (currentMargin >= 0) {
                let ialineMax = (1000 * tube.iagp) + currentMargin;  
                tube.ialineMax = ialineMax >= tube.iamax ? tube.iamax : ialineMax;
            }
        };

        /**
         * Draggable function for left Y axis, current selector
         */
        const tickIDrag = (point) => {
            if (point.y <= tube.iamax && point.y > 0) {
                var t = point.y / tube.ua;
                var u = 0.001 * (point.y - t * tube.uag1);
                var n = tube.getUg(tube.uag1, u);
                if (n < tube.ugmin) {
                    tube.ia = point.y;
                    tube.iagp = 0.001 * (tube.ia - t * tube.uagp);
                    tube.ugp = tube.getUg(tube.uagp, tube.iagp);
                    tube.iag1 = u;
                    tube.ug1 = n;
                    tube.iag2 = 0.001 * (tube.ia - t * tube.uag2);
                    tube.ug2 = tube.getUg(tube.uag2, tube.iag2);
                    tube.uinp = tube.ug1 - tube.ugp;
                    if ('P' === tube.type) {
                        tube.is = tube.getIs(tube.ugp, tube.us);
                    }

                    updateMaxCurrentLine();
                    frontLayerDraw();
                }
            }
        };


        const tickUDrag = (e) => {
            if (e.x <= tube.uamax && e.x > tube.uagp && e.x > tube.uag2) {
                var t = tube.ia / e.x;
                var u = 0.001 * (tube.ia - t * tube.uag1);
                var n = tube.getUg(tube.uag1, u);
                if (n <= tube.ugmin) {
                    tube.ua = e.x;
                    tube.iagp = 0.001 * (tube.ia - t * tube.uagp);
                    tube.ugp = tube.getUg(tube.uagp, tube.iagp);
                    tube.iag1 = u;
                    tube.ug1 = n;
                    tube.iag2 = 0.001 * (tube.ia - t * tube.uag2);
                    tube.ug2 = tube.getUg(tube.uag2, tube.iag2);
                    tube.uinp = tube.ug1 - tube.ugp;
                    if ('P' === tube.type) {
                        tube.is = tube.getIs(tube.ugp, tube.us);
                    }
                    frontLayerDraw();
                }
            }
        };

        const tickPDrag = (e) => {
            if (e.x < tube.ua && e.x > 0) {
                var t = tube.ia / tube.ua;
                var u = 0.001 * (tube.ia - t * e.x);
                var n = tube.getUg(e.x, u);
                if (n + tube.uinp < tube.ugmin) {
                    tube.uagp = e.x;
                    tube.iagp = u;
                    tube.ugp = n;
                    tube.uag1 = tube.getUa_Ra(n + tube.uinp);
                    tube.iag1 = 0.001 * (tube.ia - t * tube.uag1);
                    tube.ug1 = tube.getUg(tube.uag1, tube.iag1);
                    tube.ug2 = 2 * tube.ugp - tube.ug1;
                    tube.uag2 = tube.getUa_Ra(tube.ug2);
                    tube.iag2 = 0.001 * (tube.ia - t * tube.uag2);
                    if ('P' === tube.type) {
                        tube.is = tube.getIs(tube.ugp, tube.us);
                    }

                    updateMaxCurrentLine();
                    frontLayerDraw();
                }
            }
        };

        /**
         * Draggable function for top X axis, input voltage selector
         * @param  {??} e ??
         */
        const tickADrag = (e) => {
            //console.log('Drag X axis, input voltage selector');
            if (e.x > 0 && e.x < tube.uagp) {
                var t = tube.ia / tube.ua;
                var u = 0.001 * (tube.ia - t * e.x);
                var n = tube.getUg(e.x, u);
                if (n < tube.ugmin) {
                    tube.uinp = n - tube.ugp;
                    tube.uag1 = e.x;
                    tube.ug1 = n;
                    tube.iag1 = u;
                    tube.ug2 = 2 * tube.ugp - tube.ug1;
                    tube.uag2 = tube.getUa_Ra(tube.ug2);
                    tube.iag2 = 0.001 * (tube.ia - t * tube.uag2);
                    frontLayerDraw();
                }
            }
        };

        const tickUPDrag = (e) => {

            if (e.x <= tube.uamax && e.x > tube.uag2) {
                var t = 1e3 * tube.iagp / (e.x - tube.uagp);
                var u = t * e.x;
                if (u < tube.iamax) {
                    tube.ia = u;
                    tube.ua = e.x;
                    tube.uag1 = tube.getUa_Ra(tube.ug1);
                    tube.iag1 = 0.001 * (tube.ia - t * tube.uag1);
                    tube.uag2 = tube.getUa_Ra(tube.ug2);
                    tube.iag2 = 0.001 * (tube.ia - t * tube.uag2);
                    frontLayerDraw();
                }
            }
        };

        const tickIPDrag = (e) => {

            if (e.y <= tube.iamax && e.y > 0) {
                var t = tube.uagp / (e.y - 1e3 * tube.iagp);
                var u = t * e.y;
                if (u < tube.uamax) {
                    tube.ia = e.y;
                    tube.ua = u;
                    tube.uag1 = tube.getUa_Ra(tube.ug1);
                    tube.iag1 = 0.001 * (tube.ia - tube.uag1 / t);
                    tube.uag2 = tube.getUa_Ra(tube.ug2);
                    tube.iag2 = 0.001 * (tube.ia - tube.uag2 / t);
                    frontLayerDraw();
                }
            }
        };

        const tickIAlineMinDrag = (point) => {
            if (point.y > tube.iamin && point.y < tube.ialineMax) {
                tube.ialineMin = point.y;
                const currentMargin = (1000 * tube.iagp) - point.y;
                if (currentMargin <= 0) {
                    return;
                }
                updateMaxCurrentLine();
                frontLayerDraw();
            }

        };

        return {
            tickIDrag,
            tickUDrag,
            tickPDrag,
            tickADrag,
            tickUPDrag,
            tickIPDrag,
            tickIAlineMinDrag
        };
    };

    exposeGlobal('setupDragHandlers', setupDragHandlers);

})();