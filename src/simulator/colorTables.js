(function () {
    'use strict';
    /*globals exposeGlobal*/

    /**
     * Color scheme for the plots
     * @type {Object}
     */
    const colorTables = {
        production: {
            c_backLayer: '#303a30',
            c_tickU: '#00ffff',
            c_tickI: '#00ffff',
            c_tickP: '#ffffff',
            c_tickA: 'yellow',
            c_tickUP: '#8899ff',
            c_tickIP: '#8899ff',
            c_backGraph: '#608090',
            c_backText: '#8090a0',
            c_padm: '#ff0000',
            c_frontLoadLine: '#00ffff',
            c_frontUagpLine: '#ffffff',
            c_frontUagpCurve: '#ffffff',
            c_frontUag1Line: '#ffff00',
            c_frontUag1Curve: '#ffff00',
            c_frontUag2Line: '#ffff00',
            c_frontUag2Curve: '#ffff00',
            c_frontText: '#ffffff'
        },

        development: {
            c_backLayer: '#ffffff',
            c_tickU: '#00ffff',
            c_tickI: '#00ffff',
            c_tickP: '#000000',
            c_tickA: '#00ff00',
            c_tickUP: '#8899ff',
            c_tickIP: '#8899ff',
            c_backGraph: '#0000ff',
            c_backText: '#000000',
            c_padm: '#ff0000',
            c_frontLoadLine: '#00ffff',
            c_frontUagpLine: '#000000',
            c_frontUagpCurve: '#000000',
            c_frontUag1Line: '#00ff00',
            c_frontUag1Curve: '#00ff00',
            c_frontUag2Line: '#00ff00',
            c_frontUag2Curve: '#00ff00',
            c_frontText: '#000000'
        },

        fancy: {
            c_backLayer: '#212121',
            c_tickU: '#BBDEFB',
            c_tickI: '#BBDEFB',
            c_tickP: '#2196F3',
            c_tickA: '#8BC34A',
            c_tickUP: '#8899ff',
            c_tickIP: '#8899ff',
            c_backGraph: '#608090',
            c_backText: '#BBDEFB',
            c_padm: '#ff0000',
            c_frontLoadLine: '#BBDEFB',
            c_frontUagpLine: '#2196F3',
            c_frontUagpCurve: '#2196F3',
            c_frontUag1Line: '#8BC34A',
            c_frontUag1Curve: '#8BC34A',
            c_frontUag2Line: '#8BC34A',
            c_frontUag2Curve: '#8BC34A',
            c_frontText: '#BBDEFB',
            c_axesText: 'rgba(182, 182, 182, 0.3)',
            c_ialine: '#ff0000',
        },
    };

    exposeGlobal('colorTables', colorTables);
})();