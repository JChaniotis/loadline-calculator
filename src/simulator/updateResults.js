(function () {
    'use strict';
    /*globals exposeGlobal, domAccess*/

    /**
     * Renders resistance value and unit accordingly
     * @param  {Number} value
     * @param  {DOM element} valueElement
     * @param  {Dom element} unitElement
     */
    const renderResistance = (value, valueElement, unitElement) => {
        if (value > 1000) {
            valueElement.innerHTML = (0.001 * value).toFixed(2);
            unitElement.innerHTML = 'K&#8486;';
        } else {
            valueElement.innerHTML = value.toFixed(2);
            unitElement.innerHTML = '&#8486;';
        }
    };

    /**
     * Renders power value and unit accordingly
     * @param  {Number} value
     * @param  {DOM element} valueElement
     * @param  {Dom element} unitElement
     */
    const renderPower = (value, valueElement, unitElement) => {
        if (value > 1) {
            valueElement.innerHTML = value.toFixed(2);
            unitElement.innerHTML = 'W';
        } else {
            valueElement.innerHTML = (1000 * value).toFixed(1);
            unitElement.innerHTML = 'mW';
        }
    };

    /**
     * Renders inductance value and unit accordingly
     * @param  {Number} value
     * @param  {DOM element} valueElement
     * @param  {Dom element} unitElement
     */
    const renderInductance = (value, valueElement, unitElement) => {
        if (value > 1) {
            valueElement.innerHTML = value.toFixed(2);
            unitElement.innerHTML = 'H';
        } else {
            valueElement.innerHTML = (1000 * value).toFixed(1);
            unitElement.innerHTML = 'mH';
        }
    };

    /**
     * Renders voltage value and unit accordingly
     * @param  {Number} value
     * @param  {DOM element} valueElement
     * @param  {Dom element} unitElement
     */
    const renderVoltage = (value, valueElement, unitElement) => {
        if (value > 1) {
            valueElement.innerHTML = value.toFixed(2);
            unitElement.innerHTML = 'V';
        } else {
            valueElement.innerHTML = (1000 * value).toFixed(1);
            unitElement.innerHTML = 'mV';
        }
    };


    function updateResults(tube) {

        let Ri = 1 / (tube.getIa(tube.ugp, tube.uagp + 1) - tube.iagp);
        let S = -1000 * (tube.getIa(0.99 * tube.ugp, tube.uagp) - tube.getIa(tube.ugp, tube.uagp)) / (0.01 * tube.ugp);
        let mu = 0.001 * S * Ri;
        let Ra = 1000 * tube.ua / tube.ia;
        let Pa = tube.uagp * tube.iagp;
        

        domAccess.anodeVoltage.innerHTML = tube.uagp.toFixed(1);
        domAccess.anodeCurrent.innerHTML = (1000 * tube.iagp).toFixed(2);
        domAccess.gridVoltage.innerHTML = tube.ugp.toFixed(2);
        renderResistance(Ri, domAccess.internalResistanceValue, domAccess.internalResistanceUnit);
        domAccess.anodeLoadResistance.innerHTML = (0.001 * Ra).toFixed(1);
        domAccess.transconductance.innerHTML = S.toFixed(2);
        domAccess.amplificationFactor.innerHTML = mu.toFixed(2);
        renderPower(Pa, domAccess.anodePowerValue, domAccess.anodePowerUnit);
        domAccess.anodePowerPcent.innerHTML = (Pa/tube.padm *100).toFixed(2);
        renderVoltage(tube.uinp, domAccess.inputVoltageValue, domAccess.inputVoltageUnit);

        tube.calculateHarmonicDistortion();
        domAccess.secondHarmonic.innerHTML = (100 * tube.h2).toFixed(3);
        domAccess.thirdHarmonic.innerHTML = (100 * tube.h3).toFixed(3);
        domAccess.fourthHarmonic.innerHTML = (100 * tube.h4).toFixed(3);
        domAccess.totalHarmonic.innerHTML = (100 * tube.htot).toFixed(3);

        if ('T' === tube.type) {
            domAccess.screenCurrentContainer.hide();
            domAccess.screenPowerContainer.hide();
            domAccess.triodeOnlyResults.show();

            let Rk = -tube.ugp / tube.iagp;

            if (Rk > 0) {

                let ZoRk0 = (Ri * Ra) / (Ri + Ra);
                let Prk = tube.iagp * tube.iagp * Rk;
                let G = mu * Ra / (Ri + Ra + Rk * (mu + 1));
                let G0 = mu * Ra / (Ri + Ra);
                renderPower(Prk, domAccess.cathodeResistorPowerValue, domAccess.cathodeResistorPowerUnit);
                renderResistance(Rk, domAccess.cathodeResistorValue, domAccess.cathodeResistorUnit);
                renderResistance(ZoRk0, domAccess.bypassedOutputImpedanceValue, domAccess.bypassedOutputImpedanceUnit);
                domAccess.unBypassedGainValue.innerHTML = G.toFixed(1);
                renderVoltage(tube.uinp * G, domAccess.unBypassedVoltageValue, domAccess.unBypassedVoltageUnit);
                domAccess.bypassedGainValue.innerHTML = G0.toFixed(1);
                renderVoltage(tube.uinp * G0, domAccess.bypassedVoltageValue, domAccess.bypassedVoltageUnit);

                //unBypassedVoltageValue
                //unBypassedVoltageUnit

            } else {
                domAccess.cathodeResistorValue.innerHTML = 'x';
                domAccess.cathodeResistorPowerValue.innerHTML = 'x';
                domAccess.unBypassedGainValue.innerHTML = 'x';
                domAccess.bypassedGainValue.innerHTML = 'x';
                domAccess.bypassedOutputImpedanceValue.innerHTML = 'x';
            }

            // Note: this READS from DOM...
            let h = parseFloat(domAccess.lowFrequencyLimit.value);
            let p = parseFloat(domAccess.lowFrequencyDistortion.value);
            p = Math.pow(10, p / 20);
            let y = parseFloat(domAccess.maxFrequencyLimit.value);
            let f = parseFloat(domAccess.maxFrequencyDistortion.value);
            f = Math.pow(10, f / 20);
            let r = parseFloat(domAccess.transformerEfficiency.value);
            let l = parseFloat(domAccess.loudspeakerImpedance.value);

            let m = (tube.uag2 - tube.uag1) * (tube.iag1 - tube.iag2) / 8;
            let s = r * m;
            let c = Ra * (1 - r) / 2;
            let T = (Ri + 2 * c) * (Ra - 2 * c) / (Ra + Ri);
            let d = T / (2 * Math.PI * h * Math.sqrt(p * p - 1));
            let b = (Ra + Ri) * Math.sqrt(f * f - 1) / (2 * Math.PI * y);
            let I = Math.sqrt(l / (Ra * r));

            renderPower(m, domAccess.outputPowerValue, domAccess.outputPowerUnit);
            renderPower(s, domAccess.outputPowerCorrectedValue, domAccess.outputPowerCorrectedUnit);            
            renderInductance(d, domAccess.minInductanceValue, domAccess.minInductanceUnit);
            renderInductance(b, domAccess.maxLeakageInductanceValue, domAccess.maxLeakageInductanceUnit);
            domAccess.windingsRatio.innerHTML = I.toFixed(5);
            domAccess.reverseWindingsRatio.innerHTML = (1 / I).toFixed(1);

        } else {
            domAccess.triodeOnlyResults.hide();
            domAccess.screenCurrentContainer.show();
            domAccess.screenPowerContainer.show();
            domAccess.screenCurrentValue.innerHTML = (1e3 * tube.is).toFixed(2);
            domAccess.screenCurrentUnit.innerHTML = 'mA';
            let Ps = tube.is * tube.us;
            renderPower(Ps, domAccess.screenPowerValue, domAccess.screenPowerUnit);

        }


    }

    exposeGlobal('updateResults', updateResults);
})();