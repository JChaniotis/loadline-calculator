(function () {
    'use strict';
    /*globals exposeGlobal*/

    /*
        models: http://www.normankoren.com/Audio/Tubemodspice_article.html
                http://www.normankoren.com/Audio/Tube_params.html

        Nice case to test the models:
        http://www.shine7.com/audio/12au7_pre.htm
     */


    /**
     * Abstract tube class
     */
    class TUBE {

        constructor(tubeData) {
            // Type of tube (T or P) for triode or pentode
            this.type = tubeData[0];
            // Code name of the tube i.e 12AX7
            this.name = tubeData[1];
            // Paralleled tube sections
            this.partub = tubeData[2];
            // Amplification factor
            this.mu = tubeData[3];
            // X Exponent: rarely need to change. Not optimized if only a single value of Vs is entered for Pentodes.
            this.ex = tubeData[4];
            // Inversely proportional to overall plate current.
            this.kg1 = tubeData[5];
            // Affects opration in region of large plate voltage and large negative grid voltage
            this.kp = tubeData[6];
            // Knee volts. A good initial guess helps convergence.
            this.kvb = tubeData[7];
            // Max anode dissipation
            this.padm = tubeData[8] * tubeData[2];
            // Min anode voltage
            this.uamin = tubeData[9];
            // Max anode voltage
            this.uamax = tubeData[10];
            // Min anode current
            this.iamin = tubeData[11];
            // Max anode current
            this.iamax = tubeData[12] * tubeData[2];
            // Grid 1 min voltage
            this.ugmin = tubeData[13];
            // Grid 1 max voltage
            this.ugmax = tubeData[14];
        }

        /**
         * Getters - Setters used to remove unnecessary precision from operating params
         */

        set ia(ia) {
            this._ia = this.trimNumber(ia);
        }

        get ia() {
            return this._ia;
        }

        set ua(ua) {
            this._ua = this.trimNumber(ua);
        }

        get ua() {
            return this._ua;
        }

        set ugp(ugp) {
            this._ugp = this.trimNumber(ugp);
        }

        get ugp() {
            return this._ugp;
        }

        set ug1(ug1) {
            this._ug1 = this.trimNumber(ug1);
        }

        get ug1() {
            return this._ug1;
        }

        set ug2(ug2) {
            this._ug2 = this.trimNumber(ug2);
        }

        get ug2 () {
            return this._ug2;
        }

        set uinp(uinp) {
            this._uinp = this.trimNumber(uinp);
        }

        get uinp () {
            return this._uinp;
        }

        set uagp(uagp) {
            this._uagp = this.trimNumber(uagp);
        }

        get uagp () {
            return this._uagp;
        }

        set uag1(uag1) {
            this._uag1 = this.trimNumber(uag1);
        }

        get uag1 () {
            return this._uag1;
        }

        set uag2(uag2) {
            this._uag2 = this.trimNumber(uag2);
        }

        get uag2 () {
            return this._uag2;
        }

        set iagp(iagp) {
            this._iagp = this.trimNumber(iagp);
        }

        get iagp () {
            return this._iagp;
        }

        set iag1(iag1) {
            this._iag1 = this.trimNumber(iag1);
        }

        get iag1 () {
            return this._iag1;
        }

        set iag2(iag2) {
            this._iag2 = this.trimNumber(iag2);
        }

        get iag2 () {
            return this._iag2;
        }

        set ialineMin(ialineMin) {
            this._ialineMin = this.trimNumber(ialineMin);
        }

        get ialineMin () {
            return this._ialineMin;
        }

        set ialineMax(ialineMax) {
            this._ialineMax = this.trimNumber(ialineMax);
        }

        get ialineMax () {
            return this._ialineMax;
        }

        trimNumber (number, precision = 6) {
            return parseFloat(number.toFixed(precision));
        }


        /**
         * Approximate the maximum plate voltage for a given grid voltage
         * @param  {Number} gridVoltage
         * @return {Number}
         */
        getUaMax_Ug(gridVoltage) {
            let approximatedUa = this.uamax / 2;
            let voltageStep = approximatedUa;
            let computedIa;

            while (true) {

                computedIa = 1000 * this.getIa(gridVoltage, approximatedUa);

                // Stop when the voltage step has become small enough and
                // the approximatedUa has exceeded uamax
                if (voltageStep < 0.05 && approximatedUa >= this.uamax) {
                    return this.uamax;
                }
                // Stop when the error is small enough
                if (Math.abs(this.iamax - computedIa) < 0.000001) {
                    return approximatedUa;
                }


                // Adjust step accordingly
                if (computedIa > this.iamax) {
                    approximatedUa -= voltageStep;
                } else {
                    approximatedUa += voltageStep;
                }
                voltageStep /= 2;

            }
        }

        /*
          Approximate anode voltage for given grid voltage
         */
        getUa_Ra(gridVoltage) {

            let computedIa;
            let approximatedIa;
            let approximatedUa = this.ua / 2;
            let voltageStep = approximatedUa;

            let a = this.ia / this.ua;

            while (true) {
                // Still dont get this one....
                approximatedIa = this.ia - a * approximatedUa;
                computedIa = 1000 * this.getIa(gridVoltage, approximatedUa);

                // Stop when the error is small enough
                if (Math.abs(approximatedIa - computedIa) < 0.000001) {
                    return approximatedUa;
                }

                // Adjust step accordingly
                if (approximatedIa > computedIa) {
                    approximatedUa += voltageStep;
                } else {
                    approximatedUa -= voltageStep;
                }
                voltageStep /= 2;
            }

        }

        /**
         * The signum function, sgn(x) = 1 for x >= 0; sgn(x) = -1 for x < 0,
         * is used to prevent current flow when E1 < 0.
         * @param  {Number} x 
         * @return {Number}
         */
        sign(x) {
            return x > 0 ? 1 : 0 > x ? -1 : 0;
        }

        //old name: harm5Point
        calculateHarmonicDistortion() {
            let g = this.ia / this.ua;
            let q = (this.ug1 + this.ugp) / 2;
            let e = this.getUa_Ra(q);
            let n = 0.001 * (this.ia - g * e);
            let o = (this.ugp + this.ug2) / 2;
            let s = this.getUa_Ra(o);
            let M = 0.001 * (this.ia - g * s);
            let t = Math.sqrt((this.uagp - this.uag1) * (this.uagp - this.uag1) + 1e6 * (this.iag1 - this.iagp) * (this.iag1 - this.iagp));
            let u = Math.sqrt((this.uag2 - this.uagp) * (this.uag2 - this.uagp) + 1e6 * (this.iagp - this.iag2) * (this.iagp - this.iag2));
            let i = Math.sqrt((s - e) * (s - e) + 1e6 * (n - M) * (M - M));
            let p = Math.sqrt((e - this.uag1) * (e - this.uag1) + 1e6 * (this.iag1 - n) * (this.iag1 - n));
            let h = Math.sqrt((this.uagp - e) * (this.uagp - e) + 1e6 * (n - this.iagp) * (n - this.iagp));
            g = Math.sqrt((s - this.uagp) * (s - this.uagp) + 1e6 * (this.iagp - M) * (this.iagp - M));
            let r = Math.sqrt((this.uag2 - s) * (this.uag2 - s) + 1e6 * (M - this.iag2) * (M - this.iag2));

            this.h2 = Math.abs(0.75 * (t - u) / (t + u + i));
            this.h3 = Math.abs(0.5 * (t + u - 2 * i) / (t + u + i));
            this.h4 = Math.abs(0.25 * (3 * g - 3 * h + p - r) / (t + u + i));
            this.htot = Math.sqrt(this.h2 * this.h2 + this.h3 * this.h3 + this.h4 * this.h4);
        }

    }

    /**
     * This is the model for a Triode tube
     * @param {Array} tubeData Triode tube model data
     */
    class TRIODE extends TUBE {
        constructor(tubeData, operatingParams) {
            super(tubeData);

            if (!operatingParams) {
                return this.setDefaultOperatingParams();
            }

            this.ia = operatingParams[0];
            this.ua = operatingParams[1];
            this.ugp = operatingParams[2];
            this.ug1 = operatingParams[3];
            this.ug2 = operatingParams[4];
            this.uinp = operatingParams[5];
            this.uagp = operatingParams[6];
            this.uag1 = operatingParams[7];
            this.uag2 = operatingParams[8];
            this.iagp = operatingParams[9];
            this.iag1 = operatingParams[10];
            this.iag2 = operatingParams[11];
            this.ialineMin = operatingParams[12];
            this.ialineMax = operatingParams[13];
        }

        setDefaultOperatingParams() {
            // Anode current, 90% of maximum.
            this.ia = Math.round(0.9 * this.iamax);
            // Anode voltage, 80% of maximum.
            this.ua = Math.round(0.8 * this.uamax);

            // Grid 1 voltage 50% of maximum.
            this.ugp = 0.5 * this.ugmax;
            // Grid 1 input voltage range, left to center
            this.ug1 = 0.25 * this.ugmax;
            // Grid 1 input voltage range, center to right
            this.ug2 = 0.75 * this.ugmax;
            // Input voltage value
            this.uinp = this.ug1 - this.ugp;

            // Anode voltage
            this.uagp = this.getUa_Ra(this.ugp);
            this.uag1 = this.getUa_Ra(this.ug1);
            this.uag2 = this.getUa_Ra(this.ug2);

            // Anode current
            this.iagp = this.getIa(this.ugp, this.uagp);
            this.iag1 = this.getIa(this.ug1, this.uag1);
            this.iag2 = this.getIa(this.ug2, this.uag2);

            // Current cursors
            this.ialineMin = 0;
            this.ialineMax = 2 * (1000 * this.iagp);
        }

        serialize() {
            return {
                modelData: [
                    this.type,
                    this.name,
                    this.partub,
                    this.mu,
                    this.ex,
                    this.kg1,
                    this.kp,
                    this.kvb,
                    this.padm,
                    this.uamin,
                    this.uamax,
                    this.iamin,
                    this.iamax,
                    this.ugmin,
                    this.ugmax
                ],
                operatingParams: [
                    this.ia,
                    this.ua,
                    this.ugp,
                    this.ug1,
                    this.ug2,
                    this.uinp,
                    this.uagp,
                    this.uag1,
                    this.uag2,
                    this.iagp,
                    this.iag1,
                    this.iag2,
                    this.ialineMin,
                    this.ialineMax
                ]
            };
        }


        /**
         * Get the anode current
         * @param  {Number} Eg Grid voltage (V)
         * @param  {Number} Ep Anode voltage (V)
         * @return {Number}   Anode current (A)
         */
        getIa(Eg, Ep) {
            let E1 = Ep / this.kp * Math.log(1 + Math.exp(this.kp * (1 / this.mu + Eg / Math.sqrt(this.kvb + Ep * Ep))));
            let Ip = this.partub * Math.pow(E1, this.ex) * (1 + this.sign(E1)) / (2 * this.kg1);
            return Ip;
        }

        getUg(e, t) {
            let u = Math.exp(Math.log(t / this.partub * this.kg1) / this.ex);
            let n = Math.sqrt(this.kvb + e * e);
            let a = Math.exp(u * this.kp / e) - 1;
            let i = n * (Math.log(a) / this.kp - 1 / this.mu);
            return i;
        }

    }

    /**
     * This is the model for a Pentode tube
     * @param {Array} tubeData Pentode tube model data
     */
    class PENTODE extends TUBE {
        constructor(tubeData, operatingParams) {
            super(tubeData);

            this.us = tubeData[15];
            this.kg2 = tubeData[16];

            if (!operatingParams) {
                return this.setDefaultOperatingParams();
            }

            this.ia = operatingParams[0];
            this.ua = operatingParams[1];
            this.ugp = operatingParams[2];
            this.ug1 = operatingParams[3];
            this.ug2 = operatingParams[4];
            this.uagp = operatingParams[5];
            this.uag1 = operatingParams[6];
            this.uag2 = operatingParams[7];
            this.iagp = operatingParams[8];
            this.iag1 = operatingParams[9];
            this.iag2 = operatingParams[10];
            this.uinp = operatingParams[11];
            this.is = operatingParams[12];
            this.ialineMin = operatingParams[13];
            this.ialineMax = operatingParams[14];
        }

        set is(is) {
            this._is = this.trimNumber(is);
        }

        get is () {
            return this._is;
        }


        setDefaultOperatingParams() {
            this.ia = Math.round(0.9 * this.iamax);
            this.ua = Math.round(0.8 * this.uamax);
            this.ugp = 0.5 * this.ugmax;
            this.ug1 = 0.25 * this.ugmax;
            this.ug2 = 0.75 * this.ugmax;
            this.uagp = this.getUa_Ra(this.ugp);
            this.uag1 = this.getUa_Ra(this.ug1);
            this.uag2 = this.getUa_Ra(this.ug2);
            this.iagp = this.getIa(this.ugp, this.uagp);
            this.iag1 = this.getIa(this.ug1, this.uag1);
            this.iag2 = this.getIa(this.ug2, this.uag2);
            this.uinp = this.ug1 - this.ugp;
            this.is = this.getIs(this.ugp, this.us);
            // Current cursors
            this.ialineMin = 0;
            this.ialineMax = 2 * (1000 * this.iagp);
        }

        serialize() {
            return {
                modelData: [
                    this.type,
                    this.name,
                    this.partub,
                    this.mu,
                    this.ex,
                    this.kg1,
                    this.kp,
                    this.kvb,
                    this.padm,
                    this.uamin,
                    this.uamax,
                    this.iamin,
                    this.iamax,
                    this.ugmin,
                    this.ugmax,
                    this.us,
                    this.kg2
                ],
                operatingParams: [
                    this.ia,
                    this.ua,
                    this.ugp,
                    this.ug1,
                    this.ug2,
                    this.uagp,
                    this.uag1,
                    this.uag2,
                    this.iagp,
                    this.iag1,
                    this.iag2,
                    this.uinp,
                    this.is,
                    this.ialineMin,
                    this.ialineMax
                ]
            };
        }


        getIa(e, t) {
            let u = this.us / this.kp * Math.log(1 + Math.exp(this.kp * (1 / this.mu + e / this.us)));
            let n = this.partub * Math.pow(u, this.ex) * (1 + this.sign(u)) * Math.atan(t / this.kvb) / (1 * this.kg1);
            return n;
        }

        /**
         * Get screen grid current 
         * @param  {[type]} e [description]
         * @param  {[type]} t [description]
         * @return {[type]}   [description]
         */
        getIs(e, t) {
            let n = e + t / this.mu;
            let u = n >= 0 ? Math.pow(n, 1.5) / this.kg2 : 0;
            return u;
        }

        getUg(e, t) {
            let u = Math.exp(Math.log(t / Math.atan(e / this.kvb) / this.partub * 0.5 * this.kg1) / this.ex);
            let n = Math.exp(u * this.kp / this.us) - 1;
            let a = this.us * (Math.log(n) / this.kp - 1 / this.mu);
            return a;
        }

    }



    exposeGlobal('tubeTypes', {
        TRIODE,
        PENTODE
    });

})();