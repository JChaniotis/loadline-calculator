(function () {
    'use strict';
    /*globals exposeGlobal, domAccess, simulator, tubeModelData*/

    const selectTubeModel = () =>{
        let tubeList = domAccess.tubeList;
        let index = parseInt(tubeList.options[tubeList.selectedIndex].value);
        simulator.initialize(tubeModelData[index], null, domAccess.canvas);
    };

    exposeGlobal('selectTubeModel', selectTubeModel);
})();