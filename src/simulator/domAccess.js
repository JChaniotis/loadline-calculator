(function () {
    'use strict';
    /*globals exposeGlobal*/

    /**
     * I know that extending native elements is going to bite at some point
     * but this piece of software is still very small and manageable. 
     */
    Element.prototype.hide = function () {
        this.style.display = 'none';
    };

    Element.prototype.show = function () {
        this.style.display = null;
    };

    const domAccess = {
        // User input fields
        parallelTubes: 's_partub',
        maxAnodeVoltage: 's_uamax',
        maxAnodeCurrent: 's_iamax',
        minGridVoltage: 's_ugmin',
        maxGridVoltage: 's_ugmax',
        lowFrequencyLimit: 's_fl',
        lowFrequencyDistortion: 's_ml',
        maxFrequencyLimit: 's_fh',
        maxFrequencyDistortion: 's_mh',
        transformerEfficiency: 's_theta',
        loudspeakerImpedance: 's_rl',
        screenVoltage: 's_us',

        // Containers
        screenCurrentContainer: 'isContainer',
        triodeOnlyResults: 'triodeOnlyResults',
        screenPowerContainer: 'psContainer',

        // Parameters at operaing point
        anodeVoltage: 'uaval',
        anodeCurrent: 'iaval',
        gridVoltage: 'ugval',
        internalResistanceValue: 'rival',
        internalResistanceUnit: 'riunit',
        anodeLoadResistance: 'raval',
        transconductance: 'sval',
        amplificationFactor: 'mival',
        anodePowerValue: 'paval',
        anodePowerUnit: 'paunit',
        anodePowerPcent: 'papcent',
        screenCurrentValue: 'isval',
        screenCurrentUnit: 'isunit',
        screenPowerValue: 'psval',
        screenPowerUnit: 'psunit',

        // Harmonic distortion
        inputVoltageValue: 'uinpval',
        inputVoltageUnit: 'uinpunit',
        secondHarmonic: 'h2val',
        thirdHarmonic: 'h3val',
        fourthHarmonic: 'h4val',
        totalHarmonic: 'htotval',

        // Single stage grounded cathode
        cathodeResistorValue: 'rkval',
        cathodeResistorUnit: 'rkunit',
        cathodeResistorPowerValue: 'prkval',
        cathodeResistorPowerUnit: 'prkunit',
        bypassedOutputImpedanceValue: 'zork0val',
        bypassedOutputImpedanceUnit: 'zork0unit',
        unBypassedGainValue: 'grkval',
        unBypassedVoltageValue: 'vrkval',
        unBypassedVoltageUnit: 'vrkunit',
        bypassedVoltageValue: 'vrk0val',
        bypassedVoltageUnit: 'vrk0unit',
        bypassedGainValue: 'grk0val',


        //SE stage
        outputPowerValue: 'pouttseval',
        outputPowerUnit: 'pouttseunit',
        outputPowerCorrectedValue: 'poutseval',
        outputPowerCorrectedUnit: 'poutseunit',
        minInductanceValue: 'l1seval',
        minInductanceUnit: 'l1seunit',
        maxLeakageInductanceValue: 'l1sseval',
        maxLeakageInductanceUnit: 'l1sseunit',
        windingsRatio: 'nseval',
        reverseWindingsRatio: 'nse_revval',

        // Dropdown list
        tubeList: 'tubeType',

        // Plot canvas
        canvas: 'cvs',
    };

    // Do it with a more es6-ish way???
    Object.keys(domAccess).forEach((key) => {
        domAccess[key] = document.getElementById(domAccess[key]);
    });

    /**
     * All Dom elements that being used should be accessed through this object
     */
    exposeGlobal('domAccess', domAccess);
})();