(function () {
    'use strict';
    /*globals exposeGlobal*/
    var getLogger = function (enabled) {
        var methods = ['log', 'info', 'time', 'timeEnd', 'error', 'dir', 'warn'];


        return methods.reduce(function (logger, method) {
            if (enabled) {
                logger[method] = console[method] ? console[method].bind(console) : console.log.bind(console);
            } else {
                logger[method] = function () {};
            }
            

            return logger;
        }, {});
    };
    
    exposeGlobal('getLogger', getLogger);
})();