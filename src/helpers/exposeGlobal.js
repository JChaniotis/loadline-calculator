(function () {
    'use strict';
    const exposeGlobal = (key, val) => {
        if (window[key]) {
            throw new Error('Namespace conflict. "' + key + '" exitsts.');
        }
        window[key] = val;
    };
    // Meta n stuff
    exposeGlobal('exposeGlobal', exposeGlobal);
})();
